package fr.samadou;

/**
 * @author sareaboudousamadou.
 */
public class AfricanSpiciTea implements Beverage{
    AfricanTea africanTea;

    public AfricanTea getAfricanTea() {
        return africanTea;
    }

    public void setAfricanTea(AfricanTea africanTea) {
        this.africanTea = africanTea;
    }

    public void prepare() {
        africanTea.prepare();
        DesignUtils.talkAboutMe(this.getClass());
    }

    @Override
    public String toString() {
        return "AfricanSpiciTea{" +
                "africanTea=" + africanTea +
                '}';
    }
}
