package fr.samadou;

/**
 * @author sareaboudousamadou.
 */
public interface Beverage {
    void prepare();
}
