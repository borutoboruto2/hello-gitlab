package fr.samadou;

/**
 * @author sareaboudousamadou.
 */
public final class DesignUtils {

    private DesignUtils() {

    }

    public static void talkAboutMe(Class className) {
        System.out.println(className.toString());
    }
}
