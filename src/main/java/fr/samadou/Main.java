package fr.samadou;

/**
 * @author sareaboudousamadou.
 */
public class Main {

    public static void main(String[] args) {
        Beverage simpleGreenTea = new SimpleTea(TeaType.GREEN);
        Beverage simpleBlackTea = new SimpleTea(TeaType.BLACK);
        Beverage simpleTea = new SimpleTea(TeaType.GREEN);
        Beverage africanTea = new AfricanTea(simpleBlackTea);
        Beverage africanSpicyTea = new AfricanTea(simpleBlackTea);

        simpleTea.prepare();
        africanTea.prepare();
        africanSpicyTea.prepare();
    }

}
