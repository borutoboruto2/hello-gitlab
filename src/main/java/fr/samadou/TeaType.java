package fr.samadou;

/**
 * @author sareaboudousamadou.
 */
public enum TeaType {
    GREEN, BLACK
}
