package fr.samadou;

/**
 * @author sareaboudousamadou.
 */
public class AfricanTea implements Beverage{
    Beverage beverage;

    public AfricanTea(Beverage beverage) {
        this.beverage = beverage;
    }

    public void prepare() {
        beverage.prepare();
        DesignUtils.talkAboutMe(this.getClass());
    }

    @Override
    public String toString() {
        return "AfricanTea{" +
                "beverage=" + beverage +
                '}';
    }
}
