package fr.samadou;

/**
 * @author sareaboudousamadou.
 */
public class SimpleTea implements Beverage{
    private TeaType typeOfTea;

    public SimpleTea(TeaType typeOfTea) {
        this.typeOfTea = typeOfTea;
    }

    public TeaType getTypeOfTea() {
        return typeOfTea;
    }

    public void setTypeOfTea(TeaType typeOfTea) {
        this.typeOfTea = typeOfTea;
    }

    public void prepare() {
        DesignUtils.talkAboutMe(this.getClass());
    }

    @Override
    public String toString() {
        return "SimpleTea{" +
                "typeOfTea=" + typeOfTea +
                '}';
    }
}
